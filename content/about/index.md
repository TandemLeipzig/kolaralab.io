+++
title = "Kollektiv"
template = "page.html"

[extra]
hidetitle = "true"
+++

# Unser Ziel
„KoLaRa“, das Kollektiv Lastenrad Leipzig, möchte für möglichst viele Menschen Lastenräder durch den Bau und Verleih auf Spendenbasis nutzbar machen. 

{{ image(name="IMG_1680.jpeg") }}

# Lastenräder als nachhaltige Mobilitätslösung
Mit Lastenrädern kannst Du große Einkäufe, schwere Gegenstände und Freunde transportieren. 

## Umwelt schonen 
Lastenräder sind sauber und leise. Die meisten bei KOLARA sind recycelte, gebrauchte Fahrräder. 

## Gemeinschaftsgüter fördern  
Nichtkommerzielle solidarische Strukturen: nutzen statt besitzen  

## Selber machen 
Eigenbau und Wissensweitergabe, Vernetzung mit offenen Selbsthilfe-Werkstätten und gemeinsame Aktionen 

{{ image(name="IMG_7324.JPG") }}

# Mitmachen

## Initiative ergreifen
KOLARA ist eine offene Gruppe, ein Mitmachprojekt und freut sich über Helfer_innen! Du hast Lust, Lastenräder zu bauen oder zu warten? Auch bei der  Organisation des Kollektivs, für Promo oder Veranstaltungen kannst Du Dich einbringen. 

## Pat*in werden
Du hast ein eigenes Lastenrad und möchtest es verleihen? Dein Lastenrad erhält eine eigene E-Mail-Adresse (mit Weiterleitung auf die Mailadresse Deiner Wahl). Zudem unterstützen wir Dich beim Einstellen des Lastenrades auf dieser Website.

## Selber Bauen und Warten
Du möchtest Dir Dein eigenes Lastenrad bauen? Schweißen lernen? Dich über Konstruktion und Bauanleitungen austauschen? Dann bist Du bei uns richtig. Nimm Kontakt zu uns auf.

## Promo pro Lastenrad
Du organisierst eine Veranstaltung, auf der Leipziger Projekte und Initiven sich vorstellen und präsentieren? Wir können nicht auf jeder Feier tanzen, freuen uns aber gelegentlich in der Öffentlichkeit in Erscheinung zu treten. Schreib uns einfach eine Nachricht und wir schauen gemeinsam, was möglich ist.

## Vernetzung
Wir halten Kontakt zum [Forum Freie Lastenräder](https://dein-lastenrad.de/) und tragen zum [Lastenrad-Wiki](https://www.werkstatt-lastenrad.de/) bei. 

## Ausprobieren
Noch nie Lastenrad gefahren?  Macht nichts – es ist leicht zu lernen. Die Pat*innen helfen dir gerne beim Ausprobieren. 
{{ image(name="IMG_1234.jpeg") }}

# Kontakt aufnehmen
Um uns zu erreichen, kannst Du an die Mailingliste eine Nachricht an alle schreiben: kolara@lists.riseup.net 
Oder Du wendest Dich zunächst an: kontakt@kolara.org, um eine Einzelanfrage zu starten.
Falls Du ein Lastenrad ausleihen möchtest, wende Dich bitte direkt an die [Pat*innen der Räder.](https://kolara.gitlab.io/lastenrad/)   

