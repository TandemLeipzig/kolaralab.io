+++
title = "Die Zwei"
description = "Tandem"
template = "zeug.html"

[extra]
properties = [
  {name = "Bautyp", entry = "Tandem"},
  {name = "Standort", entry = "Krönerstraße, Anger-Crottendorf"},
  {name = "Beladung", entry = "1 Mitfahrende*r"},
  {name = "Email", entry = "diezwei@kolara.org"},
  {name = "Details", entry = "5-Gang-Nabenschaltung, Rücktrittbremse, Licht"},
]
lat = "51.339000564541260"
lon = "12.416560980240495"
+++
Für den Personentransport – auch für Personen, die nicht als Last empfunden werden

