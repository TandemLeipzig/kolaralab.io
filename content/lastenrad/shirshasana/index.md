+++
title = "Shirshasana"
description = "Fahrrad mit Frontgepäckträger"
template = "zeug.html"

[extra]
image = "image.jpg"
properties = [
  {name = "Bautyp", entry = "Frontgepäck"},
  {name = "Standort", entry = "Schirmerstraße, Anger-Crottendorf"},
  {name = "Beladung", entry = "20 kg, eine Kiste Bier und etwas zeug"},
  {name = "Email", entry = "shiri@kolara.org"},
  {name = "Details", entry = "Komplettrecycling, kurz und wendig, Schaltung und Licht"},
]
lat = "51.333812849593990"
lon = "12.410262008509047"
+++
Eine Art Bäckerrad, allerdings superleicht, schnell und wendig. Der Gepäckträger lenkt nicht mit. Es fährt sich durch die direkte Lenkung sehr leicht und ist auch für nicht Lastenrad-erprobte geeignet. Es kann auch einen Kopfstand machen.
{{ gallery() }}

