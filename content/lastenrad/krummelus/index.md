+++
title = "Krummelus"
description = "Türgestell"
template = "zeug.html"

[extra]
properties = [
  {name = "Bautyp", entry = "Anhänger"},
  {name = "Standort", entry = "Eisenbahnstraße, Radsfatz"},
  {name = "Beladung", entry = "200 kg"},
  {name = "Email", entry = "krummelus@kolara.org"},
  {name = "Details", entry = "Anhängerkupplung: 25mm Kugelkupplung (Standard an Sattelstange)"},
]
lat = "51.345187046628496"
lon = "12.412623022486837"
+++
Komplettrecycling, Sehr große Ladefläche, exzellent für große sperrige, aber eher leichte Dinge.

