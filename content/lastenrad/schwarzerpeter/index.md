+++
title = "Schwarzer Peter"
description = "Dreirad"
template = "zeug.html"

[extra]
thumbnail = "schwarzerpeter.jpg"
properties = [
  {name = "Bautyp", entry = "Dreirad"},
  {name = "Standort", entry = "Eisenbahnstraße, Radsfatz"},
  {name = "Beladung", entry = "80kg"},
  {name = "Email", entry = "schwarzerpeter@kolara.org"},
  {name = "Details", entry = "Nabenschaltung, Licht, Laderampe ausklappbar"},
]
lat = "51.34516625720686"
lon = "12.41225731983862"
+++

Der große Hebel am Lenker bremst auch. Es muss stets versucht werden, das Rad einem anderen Mitspieler unterzujubeln.
