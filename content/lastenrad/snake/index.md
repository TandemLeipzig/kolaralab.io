+++
title = "Snake"
description = "Long-John"
template = "zeug.html"

[extra]
properties = [
  {name = "Bautyp", entry = "Long-John"},
  {name = "Standort", entry = "Josephstraße, Lindenau"},
  {name = "Beladung", entry = "100 kg"},
  {name = "Email", entry = "snake@kolara.org"},
  {name = "Details", entry = "7-Gang-Nabenschaltung, Hydraulische Bremse"},
]
lat = "51.33184209040764"
lon = "12.335361071262586"
+++
Bei voller Beladung schwerfällige Lenkung
