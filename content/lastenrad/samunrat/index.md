+++
title = "Sam Unrat"
description = "Long-John"
template = "zeug.html"

[extra]
properties = [
  {name = "Bautyp", entry = "Long-John"},
  {name = "Standort", entry = "Wurzner Straße, Reudnitz"},
  {name = "Beladung", entry = "100 kg"},
  {name = "Email", entry = "sam@kolara.org"},
  {name = "Details", entry = "5-Gang-Nabenschaltung, Lenkung durch Seilzüge, wendig, Komplettrecycling"},
]
lat = "51.33812322848275"
lon = "12.406830136381185"
+++
gelb wie eine Giraffe. Das war mal ein Tisch
