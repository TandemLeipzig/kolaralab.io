+++
title = "Bäcker to the Future"
description = "Bäckerrad"
template = "zeug.html"

[extra]
properties = [
  {name = "Bautyp", entry = "Frontgepäck"},
  {name = "Standort", entry = "Wiegandstraße, Kleinzschocher"},
  {name = "Beladung", entry = "40 kg"},
  {name = "Email", entry = "baecker@kolara.org"},
  {name = "Details", entry = "Trommelbremse vorne, Rücktritt hinten, einfach und robust"},
]
lat = "51.319316390618680"
lon = "12.325768721999026"
+++
Ein professionell nostalgisches Bäckerrad. Keine Gangschaltung, aber extrem dicke Speichen im Vorderrad
