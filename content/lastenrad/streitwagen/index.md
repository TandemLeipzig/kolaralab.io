+++
title = "Streitwagen"
description = "Stahlkoloss"
template = "zeug.html"

[extra]
properties = [
  {name = "Bautyp", entry = "Anhänger"},
  {name = "Standort", entry = "Eisenbahnstraße, Radsfatz"},
  {name = "Beladung", entry = "120 kg"},
  {name = "Email", entry = "streitwagen@kolara.org"},
  {name = "Details", entry = "Anhängerkupplung: 25mm Kugelkupplung (Standard an Sattelstange)"},
]
lat = "51.3452807869177"
lon = "12.411855038186799"
+++
sehr stabil
