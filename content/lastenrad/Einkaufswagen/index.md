+++
title = "Einkaufswagen"
description = "Einkaufswagen"
template = "zeug.html"

[extra]
properties = [
  {name = "Bautyp", entry = "Hinterlader"},
  {name = "Standort", entry = "Radsfatz, Eisenbahnstraße"},
  {name = "Beladung", entry = "max. 50 kg"},
  {name = "Email", entry = "tandem-leipzig@web.de"},
  {name = "Details", entry = "Gut geeignet für großvolumige, leichte Lasten."},
]
lat = "51.345445"
lon = "12.412198"
+++
Der Einkaufswagen ist im Rahmen eines Schweißworkshops im Radsfatz entstanden und sucht zur Zeit einen neuen Paten / eine neue Patin.
