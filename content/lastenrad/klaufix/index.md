+++
title = "Klaufix"
description = "Anhänger"
template = "zeug.html"

[extra]
properties = [
  {name = "Bautyp", entry = "Anhänger"},
  {name = "Standort", entry = "Eisenbahnstraße, Radsfatz"},
  {name = "Beladung", entry = "90 kg"},
  {name = "Email", entry = "klaufix@kolara.org"},
  {name = "Details", entry = "Anhängerkupplung: 25mm Kugelkupplung (Standard an Sattelstange)"},
]
lat = "51.34524992383941"
lon = "12.412365221288322"
+++
grün, also umweltfreundlich

