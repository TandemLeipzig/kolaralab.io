+++
title = "Frau Werner"
description = "großer Frontgepäckträger"
template = "zeug.html"

[extra]
properties = [
  {name = "Bautyp", entry = "Frontgepäck"},
  {name = "Standort", entry = "Striesen, Dresden"},
  {name = "Beladung", entry = "40 kg"},
  {name = "Email", entry = "frauwerner@kolara.org"},
  {name = "Details", entry = "Kettenschaltung, unterschiedlich große Räder, Last lenkt mit"},
]
lat = "51.04925536352601"
lon = "13.78900378993156"
+++
Komplettrecycling, kurzer Rahmen, niedriger Schwerpunkt, daher extrem wendig. Erobert als erstes Kollektiv-Rad Dresden
