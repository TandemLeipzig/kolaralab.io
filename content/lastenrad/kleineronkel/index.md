+++
title = "Kleiner Onkel"
description = "Dreirad"
template = "zeug.html"

[extra]
properties = [
  {name = "Bautyp", entry = "Dreirad"},
  {name = "Standort", entry = "Eisenbahnstraße, Radsfatz"},
  {name = "Beladung", entry = "100 kg"},
  {name = "Email", entry = "kleineronkel@kolara.org"},
  {name = "Details", entry = "5-Gang-Nabenschaltung, Lenkungsdämpfer, Rücktrittbremse, Licht"},
]
lat = "51.34522203497887"
lon = "12.412201508743946"
+++
gelb
