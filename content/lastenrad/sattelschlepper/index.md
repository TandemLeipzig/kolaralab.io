+++
title = "Sattelschlepper"
description = "Long-Tail"
template = "zeug.html"

[extra]
image = "image.jpg"
properties = [
  {name = "Bautyp", entry = "Long-Tail"},
  {name = "Standort", entry = "Nordplatz, Zentrum-Nord"},
  {name = "Beladung", entry = "70 kg, drei Bierkisten"},
  {name = "Email", entry = "sattelschlepper@kolara.org"},
  {name = "Details", entry = "Komplettrecycling, langer Rahmen, nach hinten erweitert, lenkt sich wie ein nicht-Lastenrad"},
]
lat = "51.353094005853826"
lon = "12.374661533972358"
+++
eine einfaches LongTail Rad gebaut aus einem BMX -  und einem Tiefeinsteigerrahmen. Es fährt sich durch die direkte Lenkung fast wie jedes andere Fahrrad auch.
{{ gallery() }}
