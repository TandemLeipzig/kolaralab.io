+++
date = 2012-07-17
title = "Es kann losgehen!"
+++


Die ersten Gedanken sind gedacht, die ersten Texte geschrieben, Flyer gebastelt und die Verteilstruktur zumindest angeleiert.
Wie das Lollektiv Lastenrad Leipzig funktionieren kann, gestaltest du am besten selbst mit!
Es gibt mittlerweile einige die Lastenräder fahren und haben. Oft stehen  sie aber auch nur rum.
„In der Zeit könnte ich mein Rad ja auch verleihen!“
Richtig!
Dafür wollen wir ein einfach zu nutzendes Verleihsystem mit euch entwickeln.
Die Räder sollen regelmäßig über das Kollektiv gewartet werden.
Neue Räder sollen z.T. selbst gebaut oder gekauft oder geliehen werden.
Anhänger und dergleichen können über workshops auch nach und nach genutzt werden.
All das wollen  wir  auf dem Cyclo-Camp 2012 in Leipzig erstmals öffentlich machen und mit euch zusammen denken.