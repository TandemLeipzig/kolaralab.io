+++

date = 2014-10-20
title = "Lastenrad-Bauwochenende Ende Oktober"

[extra]
image = "2014-10-20.jpg"
+++


<img src="2014-10-20.jpg" alt="flyer" title="2014-10-20.jpg" />


Endlich ist es soweit: die Schwierigkeiten mit der Steuerung sind behoben und der Rahmen lackiert. So kann am Samstag, den 25. Oktober und am Sonntag, den 26. Oktober das Long-John fertig gestellt werden. Parallel wird wieder das ein oder anderen Lasti verbessert. Kommt vorbei, wir freuen uns über jede*n Mitmacher*in.
