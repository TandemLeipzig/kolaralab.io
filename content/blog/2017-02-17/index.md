+++
date = 2017-02-17
title = "Lila hat ein neues Zuhause"
+++
Lila ist umgezogen.

Lila wurde überholt und wieder fahrbereit gemacht. Mit neuer Lenkstange, nachzentrierten Rädern und frisch überholter 7-Gang-Nabe steht das Lastenrad nun in der Pörstener Straße. Gerne kann es dort wieder ausgeliehen werden.
Lila vor einigen Jahren. 

<img src="2017-02-17-01.jpg" alt="Foto" title="2017-02-17-01.jpg" />
_Lila vor einigen Jahren._

<img src="2017-02-17-02.jpg" alt="Foto" title="2017-02-17-02.jpg" />
_Lila neulich_
