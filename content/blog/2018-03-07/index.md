+++
date = 2018-03-07
title = "Schweißworkshop"
[extra]
image = "2018-03-07.png"
+++


Der Frühling steht bereit und auch dieses Jahr gibt es schon wieder diverse Lastenradprojekte. Daher wollen wir uns auch dieses Jahr wieder im Radsfatz ein Wochenende Zeit nehmen um für das Radsfatz oder KoLaRa Dinge zu schweißen.




Wann?

    Freitag 20.04. 15.00 bis ca. 19.00 Uhr
    Samstag 21.04. ab 12.00 Uhr

Ähnlich wie letztes Jahr wäre ein kleiner Spendenbeitrag von 5 – 10€ sinnvoll. Wer mitmachen möchte, meldet sich kurz bei „Frau Werner„.
