+++
date = 2017-03-07
title = "Schweißworkshop im Radsfatz"
+++
Für das kommende Frühjahr haben wir uns vorgenommen im Radsfatz einen kleinen Schweißworkshop zu veranstalten. Wir wollen Treppengeländer und Fenstergitter bauen, aber vielleicht gibts ja auch etwas für das KoLaRa zu tun.

<img src="2017-03-07.jpg" alt="Bild mit Schrott" title="2017-03-07.jpg" />

Wann?:
Freitag 7. April von bis 15.00 – 19.00 Uhr
Samstag 8. April von bis 12.00 – 19.00 Uhr

Ein kleiner Spendenbetrag von 5-10€ wäre sinnvoll um die Materialkosten zu decken. Bitte meldet euch kurz bei Frau Werner, wenn ihr euch vornehmt vorbeizukommen, damit wir das ein bisschen planen können.
