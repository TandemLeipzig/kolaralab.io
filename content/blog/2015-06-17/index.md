+++
date = 2015-06-17
title = "Eigene Mailadressen für die Lastenräder"
+++



Wir haben das Ausleihverfahren etwas geändert. Von nun an gibt es zu jedem Lastenrad eine eigene Emailadresse, die zu der jeweiligen Patin führt.

So funktioniert das Ausleihen etwas direkter und die Anfragen kommen hoffentlich schneller durch. Außerdem wird die Mailingliste entlastet.

Die Mailadressen stehen unter den Texten zu den jeweiligen Rädern. Zum Schutz vor Spam sind sie dort als Bilder eingebunden und müssen abgetippt werden.
