+++
date = 2014-03-28
title = "Lastenrad-Bauwochenenden"
[extra]
image = "img.jpg"
+++
Am zweiten und dritten Wochenende im April (11.4. bis 13.4. sowie 19.4. und 20.4.) wird im Radhaus (Josephstraße 27) vor allem am Rahmen eines neuen Long-John bebrutzelt. Parallel dazu können Lastis Probe gefahren und verschiedene kleinere Reparaturarbeiten an anderen rollenden Transportmobilen vorgenommen werden. Der Workshop ist für alle InteressentInnen offen, eine Anmeldung ist nicht erforderlich.

Los geht es am Freitag, den 11. April ab 18 Uhr mit einigen vorbereitenden Aufgaben. Am Samstag wird jeweils von 10 bis 18 Uhr und am Sonntag von 10 bis ca. 16 Uhr gebaut. Für das Mittagessen wird gesorgt. Unkostenbeitrag: Spendenbasis.


