+++
date = 2014-06-09
title = "Neuer Long-John-Rahmen fertig"

[extra]
image = "2014-06-09-1.jpg"
+++
**Neuer Long-John-Rahmen fertig**





Wie auf dem Bild unschwer zu erkennen ist: der Rahmen fürs neue Long-John ist fertig. In der nächsten Zeit geht´s ans Lackieren.
Momentan suchen wir noch nach einem Wochenende, an dem wir gemeinsam die Fahrradtechnik dran bauen und das ein oder andere Lasti weiter reparieren/tunen. Wenn auch Du dabei sein möchtest, schreibe eine Mail an kolara(klammeraffe)lists.riseup.net.

Update: es wird vorraussichtlich im Oktober wieder geschraubt.

Währenddessen sind die Lastis oft auf Tour. Hier ein paar Eindrücke:

Umzug per Lastenrad


 {{ image(name="2014-06-09-2.jpg") }}

Nicht nur Dinge lassen sich mit Siebfreak, Eblie und Lila-Lastenrad transportieren, doch schaut selbst:

{{ vimeo(id="97423652") }}



