+++
date = 2019-02-28
title = "Schweißworkshop 2019"
+++
Wie schon die letzten Jahre soll es bald wieder einen Schweißworkshop im Leipziger Osten geben. Mitte April haben wir ein Wochenende rausgesucht an dem wir das Radsfatz belegen und dort Dinge schweißen werden: 

<img src="2019-02-28.png" alt="Symbolbild" title="2019-02-28.png" />

    Fr. 12.04. 15.00 bis ca. 19.00 Uhr
    Sa. 13.04. ab 12.00 Uhr

 

Ähnlich wie letztes Jahr wäre ein kleiner Spendenbeitrag sinnvoll. Wer mitmachen möchte, melde sich kurz bei

<img src="baecker.png" alt="flyer" title="baecker.png" />


Wir haben uns auch schon einige Gedanken gemacht, was wir bauen möchten. Dazu später an dieser Stelle mehr.
