+++
date = 2013-03-01
title = "Infotreffen Lastenrad Bauworkshop"

[extra]
image = "img.jpg"
+++

Am 08. 03. machen wir in der Bäckerei / Casablanca Josephstr. 12 das Infotreffen für den ersten Bauworkshop.
Der eigentliche Bauworkshop soll dann Ende April stattfinden. Auf dem kommenden  Infotreffen wird geklärt, wer wie welches Rad bauen will und was im Rahmen des Workshops machbar sein wird.

!!! OFFEN FÜR ALLLE  !!!