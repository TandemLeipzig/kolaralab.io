+++
date = 2013-04-25
title = "Paten gesucht!"
+++
**Paten gesucht!**

Mittlerweile steht der Lastenradbauworkshop in den Nachbarschaftsgärten an und damit werden weitere Räder zur Verfügung stehen. Zwei weitere Räder und Anhänger werden in Kürze über diese Website ausleihbar sein.

Dafür suchen wir Menschen, die eine Patenschaft für ein **KoLaRa-Vehikel** übernehmen!

Was macht so ein Pate?
Nutzt das Rad ebenfalls und gibt es an Nutzer_innen nach vorheriger Absprache heraus.

MACH MIT und werde aktiv im Kollektiv Lastenrad Leipzig!
