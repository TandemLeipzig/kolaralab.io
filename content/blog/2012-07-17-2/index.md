+++
date = 2012-09-07
title = "Workshop Cyclocamp – 1. KoLaRa-Treffen"
+++
Auf dem Cyclocamp in Leipzig haben wir den ersten workshop für das KoLaRa gemacht. Die Idee dafür vorgestellt und darüber diskutiert, wie das Kollektiv sich organsieren könnte, um eine funktionierende dezentrale Verleihstruktur aufzubauen. Mittlerweile sind wir fünf Leute, die sich nun demnächst treffen, um sich noch einmal über die nächsten Schritte auszutauschen.
