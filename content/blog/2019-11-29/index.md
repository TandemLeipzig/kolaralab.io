+++
date = 2019-11-29
title = "Frau Werner empfiehlt..."
[extra]
image = "2019-11-28.png"
+++


…den Einkaufswagen bei mehr als 10 Gegenständen. Das bei dem Schweißworkshop entstandene Lastenrad hat schon einige Last transportiert und kann zu den Öffnungszeiten der Fahrradwerkstatt ausgeliehen werden. Gerade sind das

(Di, Do und So 15.00 bis 19.00 Uhr)  **!!!Coronabedingt geschlossen!!!**



Ähnlich wie der Sattelschlepper fährt es sich eher wie ein gewöhnliches Rad, sagen manche. Es hat eine 3-Gang-Schaltung, Bremse und Rücktritt und Licht. Ausleihe auf Spendenbasis.
