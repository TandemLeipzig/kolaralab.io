+++
date = 2018-10-15
title = "Neue Räder und die Abteilung Anhänger"
[extra]
image = "2018-10-15-2.jpeg"
+++
Auch wenn man hier nicht so häufig Veränderungen sieht bekommen wir ständig neue Räder zur Verfügung gestellt. So aktuell ein altes Postrad im Osten (die Ladefläche vorne wurde mittels Hammer auf Bierkastenmaß genormt):



und drei Fahrradanhänger (hier einer für die Südvorstadt):
 
 {{ image(name="2018-10-15-2.jpeg") }}

Da die Räder immer mehr werden, ist es an der Zeit unsere Liste etwas zu sortieren, ihr findet daher ab jetzt die Kategorie ‚Anhänger‚. Das ist doch schonmal ein Anfang…
