+++
date = 2016-11-05
title = "Liste aktualisiert"
+++

In der Liste der ausleihbaren Räder hatten sich einige Räder angesammelt, die nur noch in unserer Erinnerung existieren oder auf eine Wiederbelebung warten.

Murdock, Eblie und Krokodil sind nun nicht mehr in der Liste. Ausgeliehen werden konnten sie schon länger nicht mehr.



<img src="2016-11-5-1.jpg" alt="flyer" title="2016-11-05-1.jpg" />
_Murdock liegt zerlegt in der Ecke._




<img src="2016-11-05-2.jpg" alt="flyer" title="2016-11-05-2.jpg" />
_Eblie wurde geklaut._





<img src="2016-11-05-03.jpg" alt="flyer" title="2016-11-05-03.jpg" />
_Krokodil sollte schon seit langem Umgebaut werden._
