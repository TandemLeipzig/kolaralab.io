+++
date = 2013-06-12   
title = "Lastenradbau-Wochenende 20. und 21. Juli 2013"
[extra]
image = "img.jpg"
+++
**Lastenradbau-Wochenende 20. und 21. Juli 2013**

An diesem Wochenende vollenden wir das Lastendreirad „Siebfreak“ (Offensiwe) im Radhaus der Nachbarschaftsgärten (Josephstraße 27). Am Sonntag, den 21. Juli ab 12 Uhr laden wir dazu ein, verschiedene rollende Transportmöglichkeiten kennenzulernen und Probe zu fahren. Wer mag, kann gern sein/ihr Lastenfahrrad und/oder was zum gemeinsamen Essen/Trinken mitbringen.

Kommt vorbei, bringt Freunde mit und lernt uns und das KoLaRa Leipzig kennen!
