+++
date = 2019-03-25
title = "Huckepack"
+++
Es gibt viele Ansätze Lastenräder so zu bauen, dass man damit große und schwere Dinge zu transportieren kann. Besonders zwei Bauweisen haben sich über die Zeit durchgesetzt und sind auch bei uns am meisten vertreten: der Long-John und das Dreirad mit der Ladefläche jeweils vorn. Das führt dazu, dass besonders die Lenkung und damit auch das Fahrgefühl sich stark von anderen Rädern unterscheidet. Deshalb haben wir zuletzt auch versucht, andere Konzepte umzusetzen. Ein besonders Einfaches führte dabei zum Sattelschlepper

Die Grundlage hierfür bilden zwei Fahrradrahmen, bei denen einer den gesamten Hinterbau verliert und statt dessen um einen kleinen Rahmen erweitert wird. Ein BMX Rahmen bietet sich an, weil der für hohe Belastungen ausgelegt ist.

<img src="huckepack1.png" alt="flyer" title="huckepack1.png" />

Die Tretlager beider Rahmen wurden dann zusätzlich mit einer Strebe verbunden. Außerdem haben wir die Sattelstütze noch etwas stabilisiert. So ist der Rahmen aus nur 4 Einzelteilen schnell zusammengesetzt.

Jetzt fehlt nur noch die Ladefläche, wir  haben uns wieder am Bierkastenformat orientiert:

<img src="2019-03-25-2.png" alt="flyer" title="2019-03-25-2.png" />

und das ganze dann nochmal zur Sattelstütze verstärkt:

<img src="2019-03-25-3.png" alt="flyer" title="2019-03-25-3.png" />

So haben wir mit relativ wenig Aufwand ein stabiles Lastenrad gebaut. Dadurch, dass am Vorderrad nichts verändert wurde, fährt es sich fast wie die meisten anderen Fahrräder.



<img src="2019-03-25-4.png" alt="flyer" title="2019-03-25-4.png" />

Zum Schweißworkshop im April haben wir uns nun vorgenommen noch ein paar dieser Räder zu bauen und uns ein paar andere Konzepte für die Ladefläche zu überlegen. z.B. eine Schubkarre, eine Wanne oder ein alter Einkaufswagen wären was. Spitze wäre es auch ein Tandem-Lastenrad-Hybrid zu bauen.

Habt ihr noch Vorschläge oder Ideen?

