+++
date = 2014-02-25 
title = "Lastenrad-Frühjahrsputz und Planungen für 2014"


[extra]
image = "Kolara25.02.2014.jpg"
+++

Es geht wieder los! Neben den Planungen für größere Bauvorhaben gibt es am Freitag, den **14. März ab 15 Uhr** einen **Lasti-Frühjahrsputz im Radhaus (Josephstraße 27**). Hier sollen vor allem kleinere Wartungsarbeiten wie Schaltung einstellen, Bremsen nachjustieren u.s.w. angegangen werden. Kommt vorbei und nutzt die ersten Frühlings-Sonnen-Strahlen um die Lastis zu begutachten, zu fachsimpeln und rumzubauen.

Zudem wird in diesem Jahr ab April wieder mindestens ein Lasti gebaut. Im Sommer ist dann ein Weiterführungs-Workshop im Schweißen geplant, in dessen Rahmen ein Ofen für die Werkstatt entstehen soll. Die genauen Termine stehen noch nicht fest.

Um die kommenden Aktionen zu planen und sich in lockerer, offener Runde auszutauschen, gibt es ab jetzt jeden **3. Montag im Monat um 20 Uhr im Radhaus** (bei penetranter Kälte im Casa) ein Treffen. Das nächste Planungs-Plenum findet am **17. März** statt.

Wenn Du Lust hast, bei der kollektiven Nutzung, Wartung und dem Bau von Lastenrädern in Leipzig mitzuwirken, komm doch mal in die Fahrradselbsthilfe-Werkstatt Radhaus, Josephstraße 27, vorbei oder schreib uns eine Mail (kolara@lists.riseup.net). Wir sind eine kleine, chaotische und hierarchiefreie Gruppe, die vor allem eins will: grenzenlose Mobilität für alle;-)!