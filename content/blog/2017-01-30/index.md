+++
date = 2017-01-30
title = "Neue Räder im Osten"
+++

Es gibt mal wieder Nachwuchs in der Lastenradfamilie. Zwei sehr unterschiedliche Gestalten sind kürzlich hinzugekommen.

Die wendige „Frau Werner“ ist schon vielmehr Fahrrad, als Lastenrad. Die tiefe Ladefläche, mit dem Gewicht an der Gabel, lässt das kompakte Rad Lasten elegant umher fahren. Der Anhänger „Krummelus“ macht dagegen ein ganz andere Figur. Ohne Zugrad geht hier nix und auch von kompakt kann keine Rede sein. Ursprünglich als mobiles Hindernis gebaut, entwickelte der Anhänger sich zunehmend zu einem nützlichen Transportgerät für besonders große Lasten. Mit „Krummelus“ hat ein Wesen, das schon länger in der Nähe des Radsfatzes anzutreffen ist, nun einen Namen und eine Patenschaft gefunden.

Ab sofort sind sie hier auszuleihen.

<img src="2017-01-30-01.jpg" alt="Foto von Krummelus" title="2017-01-30-01.jpg" />
<img src="2017-01-30-02.jpg" alt="Foto von Frau Werner" title="2017-01-30-02.jpg" />
