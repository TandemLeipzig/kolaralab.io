+++
title = "Kontakt"

[extra]
hidetitle = 'true'
+++
# Kontakt aufnehmen
{{image(name="P6023739.JPG")}}

Um uns zu erreichen, kannst Du an über die Mailingliste eine Nachricht an alle schreiben: kolara@lists.riseup.net Wenn Du diese Mailinglist, über die wir uns in unregelmäßigen Abständen austauschen, abonieren möchtest, schreibe eine Mail an kolara-subscribe@lists.riseup.net.

Oder Du wendest Dich zunächst an: kontakt@kolara.org, um eine Einzelanfrage zu starten.

Falls Du ein Lastenrad ausleihen möchtest, wende Dich bitte direkt an die [Pat*innen der Räder.](url)  

Diese Seite wurde mit gitlab erstellt und ist in Markdown geschrieben. Wenn Du Änderungen einreichen möchtest, ...

