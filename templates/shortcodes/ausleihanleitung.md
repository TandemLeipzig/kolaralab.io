___
## So kannst du das Rad ausleihen
1. Schick deine Anfrage (inkl. Zeitraum der Ausleihe und Telefonnummer) an die Mailadresse des Rades.
2. Die Radpatinnen melden sich so bald als möglich, damit ihr euch treffen könnt.
3. Los gehts! Bitte gehe sorgsam mit dem Fahrzeug um, für Deine Fahrt bist Du selbst verantwortlich.
4. Die Verantwortliche nimmt Fahrzeug wieder entgegen. Bitte teile ihr auftretende Mängel mit, damit alle risikofrei fahren können.

_Das Kollektiv ist kein Dienstleister und agiert auf nicht kommerzieller Basis. Meldet euch daher bitte rechtzeitig._
___
